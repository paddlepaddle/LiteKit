//
//  LiteKitOCRResult.h
//  LiteKitOCR
//
//  Created by Wang,Zhiyong on 2021/6/10.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN



@interface LiteKitOCRResult : NSObject
@property (nonatomic, readonly) NSArray *points;
@property (nonatomic, readonly) NSArray<NSNumber *> *wordIndex;
@property (nonatomic, strong) NSString *label;
@property (nonatomic, assign) float confidence;

- (void)addPoints:(int)x y:(int)y;
- (void)addWordIndex:(int)index;

- (CGPoint)pointAtIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
