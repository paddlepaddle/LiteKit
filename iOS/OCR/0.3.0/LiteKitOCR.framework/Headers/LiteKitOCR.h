//
//  LiteKitOCR.h
//  LiteKitOCR
//
//  Created by Wang,Zhiyong on 2021/6/9.
//

#import <Foundation/Foundation.h>

//! Project version number for LiteKitOCR.
FOUNDATION_EXPORT double LiteKitOCRVersionNumber;

//! Project version string for LiteKitOCR.
FOUNDATION_EXPORT const unsigned char LiteKitOCRVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LiteKitOCR/PublicHeader.h>
#import <LiteKitOCR/LiteKitOCRPredictor.h>
#import <LiteKitOCR/LiteKitOCRResult.h>

