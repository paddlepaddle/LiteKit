//
//  LiteKitOCRPredictor.h
//  LiteKitOCR
//
//  Created by Wang,Zhiyong on 2021/6/9.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreVideo/CoreVideo.h>
#import "LiteKitOCRResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiteKitOCRPredictor : NSObject

/**
 * @desc create OCR
 * @param error error while create, if succeed will be nil, nullable
 * @return instancetype OCR predictor created
 */
+ (instancetype) createOCRPredictorWithError:(NSError **)error;

#pragma mark - LiteKitOCRPredictor/Infer-Sync

/**
 * @desc Portrait Segmentor inference with UIImage
 * @param image UIImage to inference
 * @param error  Error while inference, will be nil if succed
 * @return LiteKitPSData output data, size of 192*192
 */
- (NSArray<LiteKitOCRResult *> *)recognizeWithImage:(UIImage *)image
                          error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
