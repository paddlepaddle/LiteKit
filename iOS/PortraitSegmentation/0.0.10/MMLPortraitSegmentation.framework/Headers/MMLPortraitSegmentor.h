//
//  MMLPortraitSegmentor.h
//  MMLPortraitSegmentation
//
//  Created by Baidu on 2020/7/14.
//  Copyright © 2020 Baidu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>


#ifndef MMLPortraitSegmentor_h
#define MMLPortraitSegmentor_hx

NS_ASSUME_NONNULL_BEGIN

#pragma mark - MMLPortraitSegmentor/class
@class UIImage;

/**
 * @desc PortraitSegmentor data for output
 */
typedef struct MMLPortraitSegmentorData {
    struct MMLPortraitSegmentorDataShape {
        int n; // batch, =1
        int c; // channel, =1
        int h; // output height, =192
        int w; // output width, =192
    } dataShape; // output data shape
    uint8_t *data; // output data
} MMLPSData;

/**
 * @desc MML Portrait Segmentor Implement
 */
@interface MMLPortraitSegmentor : NSObject

#pragma mark - MMLPortraitSegmentor/Create

/**
 * @desc create Portrait Segmentor
 * @param error error while create, if succeed will be nil, nullable
 * @return instancetype Portrait Segmentor created
 */
+ (instancetype) createPortraitSegmentorWithError:(NSError **)error;


#pragma mark - MMLPortraitSegmentor/Infer-Sync

/**
 * @desc Portrait Segmentor inference with RawData
 * @param rawData rawData to inference
 * @param width input Data width
 * @param height input data height
 * @param error  Error while inference, will be nil if succed
 * @return MMLPSData output data, size of 192*192
 */
- (MMLPSData *) inferWithRawData:(uint8_t *)rawData
                           width:(int)width
                          height:(int)height
                           error:(NSError **)error;

/**
 * @desc Portrait Segmentor inference with UIImage
 * @param image UIImage to inference
 * @param error  Error while inference, will be nil if succed
 * @return MMLPSData output data, size of 192*192
 */
- (MMLPSData *) inferWithImage:(UIImage *)image
                         error:(NSError **)error;

/**
 * @desc Portrait Segmentor inference with CVPixelBufferRef
 * @param pixelBuffer CVPixelBufferRef to inference
 * @param error  Error while inference, will be nil if succed
 * @return MMLPSData output data, size of 192*192
 */
- (MMLPSData *) inferWithPixelBuffer:(CVPixelBufferRef)pixelBuffer
                               error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
#endif /* MMLPortraitSegmentor_h */
