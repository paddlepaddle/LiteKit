//
//  MMLPortraitSegmentation.h
//  MMLPortraitSegmentation
//
//  Created by Wang,Zhiyong on 2020/7/27.
//  Copyright © 2020 BaiduRD. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MMLPortraitSegmentation.
FOUNDATION_EXPORT double MMLPortraitSegmentationVersionNumber;

//! Project version string for MMLPortraitSegmentation.
FOUNDATION_EXPORT const unsigned char MMLPortraitSegmentationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MMLPortraitSegmentation/PublicHeader.h>

#import <MMLPortraitSegmentation/MMLPortraitSegmentor.h>
