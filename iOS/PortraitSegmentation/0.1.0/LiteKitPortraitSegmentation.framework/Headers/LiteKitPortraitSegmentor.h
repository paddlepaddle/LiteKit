/**
Copyright © 2020 Baidu, Inc. All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>


#ifndef LiteKitPortraitSegmentor_h
#define LiteKitPortraitSegmentor_h

NS_ASSUME_NONNULL_BEGIN

#pragma mark - LiteKitPortraitSegmentor/class
@class UIImage;

/**
 * @desc PortraitSegmentor data for output
 */
typedef struct LiteKitPortraitSegmentorData {
    struct LiteKitPortraitSegmentorDataShape {
        int n; // batch, =1
        int c; // channel, =1
        int h; // output height, =192
        int w; // output width, =192
    } dataShape; // output data shape
    uint8_t *data; // output data
} LiteKitPSData;

/**
 * @desc LiteKit Portrait Segmentor Implement
 */
@interface LiteKitPortraitSegmentor : NSObject

#pragma mark - LiteKitPortraitSegmentor/Create

/**
 * @desc create Portrait Segmentor
 * @param error error while create, if succeed will be nil, nullable
 * @return instancetype Portrait Segmentor created
 */
+ (instancetype) createPortraitSegmentorWithError:(NSError **)error;


#pragma mark - LiteKitPortraitSegmentor/Infer-Sync

/**
 * @desc Portrait Segmentor inference with RawData
 * @param rawData rawData to inference
 * @param width input Data width
 * @param height input data height
 * @param error  Error while inference, will be nil if succed
 * @return LiteKitPSData output data, size of 192*192
 */
- (LiteKitPSData *) inferWithRawData:(uint8_t *)rawData
                           width:(int)width
                          height:(int)height
                           error:(NSError **)error;

/**
 * @desc Portrait Segmentor inference with UIImage
 * @param image UIImage to inference
 * @param error  Error while inference, will be nil if succed
 * @return LiteKitPSData output data, size of 192*192
 */
- (LiteKitPSData *) inferWithImage:(UIImage *)image
                         error:(NSError **)error;

/**
 * @desc Portrait Segmentor inference with CVPixelBufferRef
 * @param pixelBuffer CVPixelBufferRef to inference
 * @param error  Error while inference, will be nil if succed
 * @return LiteKitPSData output data, size of 192*192
 */
- (LiteKitPSData *) inferWithPixelBuffer:(CVPixelBufferRef)pixelBuffer
                               error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
#endif /* LiteKitPortraitSegmentor_h */
