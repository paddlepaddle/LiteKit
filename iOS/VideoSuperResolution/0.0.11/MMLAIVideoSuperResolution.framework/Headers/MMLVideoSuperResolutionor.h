//
//  MMLVideo_SuperResolution.h
//  MMLVideoSuperResolution
//
//  Created by Baidu Co.,Ltd. on 2020/7/10.
//  Copyright © 2020 Baidu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MMLYUV420DataStruct.h"

NS_ASSUME_NONNULL_BEGIN

/**
* creat Video superresolution inferencer
*/
@interface MMLVideoSuperResolutionor : NSObject

/**
 * @brief initialize and return a instance of video super resolutionor
 *
 * @param error if an error occurs, this param will carry the information
 * @return a instance of gesture detector
 */
+ (instancetype)createVideoSuperResolutionorWithError:(NSError **)error;


/**
 * @brief execute videoSuperSolution
 *
 * @param inputData input data, struct MMLYUVData
 * @param outputData output data
 * @param error errorcode and error reason
 * @return BOOL YES for succeed，NO for failure
 */
- (BOOL)superResolutionWithInputData:(const MMLYUV420Data *)inputData
                          outputData:(MMLYUV420Data *)outputData
                               error:(NSError **)error;


/**
 * @brief execute videoSuperSolution
 *
 * @param inputImage input image, UIImage
 * @param error errorcode and error reason
 * @return UIImage image after superresolution
 */
- (UIImage *)superResolutionWithUIImage:(UIImage *)inputImage
                                  scale:(CGFloat)scale
                                  error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
