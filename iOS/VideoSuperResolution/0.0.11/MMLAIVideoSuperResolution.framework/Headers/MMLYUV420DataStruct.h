//
//  MMLYUV420DataStruct.h
//  MMLVideoSuperResolution
//
//  Created by Baidu Co.,Ltd. on 2020/7/10.
//  Copyright © 2020 Baidu. All rights reserved.
//

#ifndef MMLYUV420DataStruct_h
#define MMLYUV420DataStruct_h

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

///video frame YUV420 data struct
typedef struct {
    // frame width
    int width;
    // frame height
    int height;
    // data light (include Padding)
    int y_stride;
    int u_stride;
    int v_stride;
    //YUVdata
    uint8_t* y_data;
    uint8_t* u_data;
    uint8_t* v_data;
} MMLYUV420Data;//video 420 frame data

/**
 *release MMLYUVData
 * @param data input数据
 */
void releaseYUV420Data(MMLYUV420Data *data);

/**
 * copy MMLYUVData
 * @param src input data
 * @param dst output data
 */
void copyYUV420Data(const MMLYUV420Data *src, MMLYUV420Data *dst);

#ifdef __cplusplus
}  // extern "C"
#endif  // __cplusplus

#endif /* MMLYUV420DataStruct_h */
