//
//  MMLVideoSuperResolution.h
//  MMLVideoSuperResolution
//
//  Created by Baidu Co.,Ltd. on 2019/11/15.
//  Copyright © 2019 Baidu. All rights reserved.
//

#import <Foundation/Foundation.h>

#if __has_include(<MMLVideoSuperResolution/MMLVideoSuperResolution.h>)
FOUNDATION_EXPORT double MMLVideoSuperResolutionVersionNumber;
FOUNDATION_EXPORT const unsigned char MMLVideoSuperResolutionVersionString[];
// you should import all the public headers of your framework using statements like #import <MMLVideoSuperResolution/PublicHeader.h>

/// Video Super Resolution
#import <MMLVideoSuperResolution/MMLVideoSuperResolutionor.h>
#import <MMLVideoSuperResolution/MMLYUV420DataStruct.h>


#else
// you should also import all the public headers of your library using statements like #import "PublicHeader.h"
/// Video Super Resolution
#import "MMLVideoSuperResolutionor.h"
#import "MMLYUV420DataStruct.h"

#endif
