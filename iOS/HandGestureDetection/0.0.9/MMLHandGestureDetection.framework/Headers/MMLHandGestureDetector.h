//
//  MMLGestureRecognizer.h
//  MMLGestureRecognizer
//
//  Created by MML on 2020/6/23.
//  Copyright © 2020 baidu. All rights reserved.
//

#pragma once

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>

//! Project version number for MMLHandGestureDetection.
FOUNDATION_EXPORT double MMLHandGestureDetectionVersionNumber;

//! Project version string for MMLHandGestureDetection.
FOUNDATION_EXPORT const unsigned char MMLHandGestureDetectionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MMLHandGestureDetection/PublicHeader.h>
#import <MMLHandGestureDetection/MMLHandGestureDetector.h>


/**
 * define hand gesture detection result label
 */
typedef NS_OPTIONS(NSUInteger, MMLHandGestureDetectionLabel) {
    MMLHandGestureDetectionLabelHand      = 0,
    MMLHandGestureDetectionLabelFive      = 1,
    MMLHandGestureDetectionLabelVictory   = 2,
    MMLHandGestureDetectionLabelFist      = 3,
    MMLHandGestureDetectionLabelOne       = 4,
    MMLHandGestureDetectionLabelOK        = 5,
};


/**
 * define hand gesture detect result data
 */
@interface MMLHandGestureDetectResult : NSObject

// rect of hand‘s bounding box
@property (nonatomic, assign, readonly) CGRect handBoxRect;

// position of finger
@property (nonatomic, assign, readonly) CGPoint fingerPoint;

// label of detection result
@property (nonatomic, assign, readonly) MMLHandGestureDetectionLabel label;

// confidence of detection result
@property (nonatomic, assign, readonly) float confidence;

@end


/**
 * hand gesture detector
 */
@interface MMLHandGestureDetector : NSObject

/**
 * @brief initialize and return a instance of gesture detector, the model should be put in main bundle
 *
 * @param error if an error occurs, this param will carry the information
 * @return a instance of gesture detector
 */
+ (instancetype)createGestureDetector:(NSError **)error;

/**
 * @brief initialize and return a instance of gesture detector with model path
 *
 * @param path the folder where the model is located
 * @param error if an error occurs, this param will carry the information
 * @return a instance of gesture detector
 */
+ (instancetype)createGestureDetectorWithModelPath:(NSString *)path error:(NSError **)error;

/**
 * @brief detect hand gesture with image
 *
 * @param image UIImage that will be detected
 * @param complete completion block with result and error imformation
 */
- (void)detectWithUIImage:(UIImage *)image
                 complete:(void (^)(MMLHandGestureDetectResult *result, NSError *error))complete;

/**
 * @brief detect hand gesture with sampleBuffer
 *
 * @param sampleBuffer sampleBuffer that will be detected
 * @param complete completion block with result and error imformation
 */
- (void)detectWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
                      complete:(void (^)(MMLHandGestureDetectResult *result, NSError *error))complete;

/**
 * @brief detect hand gesture with pixelRawData
 *
 * @param pixelRawData pixelRawData that will be detected, pixel format shoud be RGBA
 * @param width detect buffer width
 * @param height detect buffer height
 * @param complete completion block with result and error imformation
 */
- (void)detectWithPixelRawData:(uint8_t *)pixelRawData
                         width:(int)width
                        height:(int)height
                      complete:(void (^)(MMLHandGestureDetectResult *result, NSError *error))complete;

@end
