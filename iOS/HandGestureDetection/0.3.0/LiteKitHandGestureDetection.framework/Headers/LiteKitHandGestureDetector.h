/**
Copyright © 2020 Baidu, Inc. All Rights Reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>

//! Project version number for LiteKitHandGestureDetection.
FOUNDATION_EXPORT double LiteKitHandGestureDetectionVersionNumber;

//! Project version string for LiteKitHandGestureDetection.
FOUNDATION_EXPORT const unsigned char LiteKitHandGestureDetectionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LiteKitHandGestureDetection/PublicHeader.h>
#import <LiteKitHandGestureDetection/LiteKitHandGestureDetector.h>


/**
 * define hand gesture detection result label
 */
typedef NS_OPTIONS(NSUInteger, LiteKitHandGestureDetectionLabel) {
    LiteKitHandGestureDetectionLabelHand      = 0,
    LiteKitHandGestureDetectionLabelFive      = 1,
    LiteKitHandGestureDetectionLabelVictory   = 2,
    LiteKitHandGestureDetectionLabelFist      = 3,
    LiteKitHandGestureDetectionLabelOne       = 4,
    LiteKitHandGestureDetectionLabelOK        = 5,
};


/**
 * define hand gesture detect result data
 */
@interface LiteKitHandGestureDetectResult : NSObject

// rect of hand‘s bounding box
@property (nonatomic, assign, readonly) CGRect handBoxRect;

// position of finger
@property (nonatomic, assign, readonly) CGPoint fingerPoint;

// label of detection result
@property (nonatomic, assign, readonly) LiteKitHandGestureDetectionLabel label;

// confidence of detection result
@property (nonatomic, assign, readonly) float confidence;

@end


/**
 * hand gesture detector
 */
@interface LiteKitHandGestureDetector : NSObject

/**
 * @brief initialize and return a instance of hand gesture detector, the model should be put in main bundle
 *
 * @param error if an error occurs, this param will carry the information
 * @return a instance of gesture detector
 */
+ (instancetype)createHandGestureDetectorWithError:(NSError **)error;

/**
 * @brief detect hand gesture with image
 *
 * @param image UIImage that will be detected
 * @param complete completion block with result and error imformation
 */
- (void)detectWithUIImage:(UIImage *)image
                 complete:(void (^)(LiteKitHandGestureDetectResult *result, NSError *error))complete;

/**
 * @brief detect hand gesture with sampleBuffer
 *
 * @param sampleBuffer sampleBuffer that will be detected
 * @param complete completion block with result and error imformation
 */
- (void)detectWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
                      complete:(void (^)(LiteKitHandGestureDetectResult *result, NSError *error))complete;

/**
 * @brief detect hand gesture with pixelRawData
 *
 * @param pixelRawData pixelRawData that will be detected, pixel format shoud be RGBA
 * @param width detect buffer width
 * @param height detect buffer height
 * @param complete completion block with result and error imformation
 */
- (void)detectWithPixelRawData:(uint8_t *)pixelRawData
                         width:(int)width
                        height:(int)height
                      complete:(void (^)(LiteKitHandGestureDetectResult *result, NSError *error))complete;

@end
