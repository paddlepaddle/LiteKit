//
//  LiteKitImageUtil.h
//  LiteKitOCR
//
//  Created by Wang,Zhiyong on 2021/6/14.
//

#include <opencv2/opencv.hpp>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiteKitImageUtil : NSObject

+ (cv::Mat)CVMatFromUIImage:(UIImage *)image;

+ (cv::Mat)convertRGBA2BGR:(cv::Mat)inputImage;

+ (UIImage*)resizeImage:(UIImage *)originImage toSize:(CGSize)size;

+ (UIImage *)resizeImage:(UIImage *)originImage withMaxSideLength:(long)maxLength withStep:(int)step;
@end

NS_ASSUME_NONNULL_END
